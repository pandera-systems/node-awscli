# Supported tags

- `8.7.0`, `8.7`, `8`, `latest` [(node-8.7.0/Dockerfile)](https://bitbucket.org/pandera-systems/node-awscli/src/master/node-8.7.0/Dockerfile)
- `8.7.0-alpine`, `8.7-alpine`, `8-alpine`, `alpine` [(node-8.7.0/alpine/Dockerfile)](https://bitbucket.org/pandera-systems/node-awscli/src/master/node-8.7.0/alpine/Dockerfile)
- `1.0.0` *DEPRECATED*

# Quick Reference

-	**Maintained by**:  
	[Pandera Systems, LLC.](http://panderasystems.com/)

- **Where to file issues:**
  [BitBucket](https://bitbucket.org/pandera-systems/node-awscli/issues)
